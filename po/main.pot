# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-06-19 17:36+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/modules/containerMenuIcons.js:27 src/modules/containerMenuText.js:22
msgid "Attach Shell"
msgstr ""

#: src/modules/containerMenuIcons.js:31 src/modules/containerMenuText.js:26
msgid "Pause"
msgstr ""

#: src/modules/containerMenuIcons.js:35 src/modules/containerMenuIcons.js:50
#: src/modules/containerMenuText.js:30 src/modules/containerMenuText.js:44
msgid "Stop"
msgstr ""

#: src/modules/containerMenuIcons.js:39 src/modules/containerMenuIcons.js:54
#: src/modules/containerMenuText.js:34 src/modules/containerMenuText.js:48
msgid "Restart"
msgstr ""

#: src/modules/containerMenuIcons.js:46 src/modules/containerMenuText.js:40
msgid "Unpause"
msgstr ""

#: src/modules/containerMenuIcons.js:60 src/modules/containerMenuText.js:54
msgid "Start"
msgstr ""

#: src/modules/containerMenuIcons.js:64 src/modules/containerMenuText.js:58
msgid "Start interactive"
msgstr ""

#: src/modules/containerMenuIcons.js:69 src/modules/containerMenuText.js:64
msgid "Remove"
msgstr ""

#: src/modules/imageMenuIcons.js:24 src/modules/imageMenuText.js:19
msgid "Run"
msgstr ""

#: src/modules/imageMenuIcons.js:28 src/modules/imageMenuText.js:23
msgid "Run interactive"
msgstr ""

#: src/modules/imageMenuIcons.js:32 src/modules/imageMenuText.js:27
msgid "Remove (force)"
msgstr ""

#: src/modules/menu.js:22
msgid "Docker Menu"
msgstr ""
