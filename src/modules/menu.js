'use strict';

const { Gio, GObject, St } = imports.gi;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Me_PopupMenu = Me.imports.modules.popupMenu;

const ContainerMenuText = Me.imports.modules.containerMenuText;
const ContainerMenuIcons = Me.imports.modules.containerMenuIcons;
const ImageMenuText = Me.imports.modules.imageMenuText;
const ImageMenuIcons = Me.imports.modules.imageMenuIcons;

const Docker = Me.imports.lib.docker;


var Menu = GObject.registerClass(
	class Menu extends PanelMenu.Button {
		_init() {
			super._init(0.0, _('Docker Menu'));
			this.settings = ExtensionUtils.getSettings('org.gnome.shell.extensions.docker');

			// Add icon
			let gicon = Gio.icon_new_for_string(Me.path + `/resources/docker_${this.settings.get_string('logo')}.png`);
			let icon = new St.Icon({ gicon: gicon, icon_size: '24' });
			this.add_child(icon);

			this.connect('button-press-event', async () => {
				this.menu.removeAll();
				await this._show();
				this.menu.open();
			});
		}

		async _show() {
			// Check if docker is running
			let is_running = await Docker.is_docker_running();
			if (!is_running) {
				// Docker is not running.
				// Add button start
				this.menu.addAction(Docker.docker_commands.s_start.label, () => {
					Docker.run_command(Docker.docker_commands.s_start);
				});
				return;
			}

			// Docker is running.
			this._scroll_section = new Me_PopupMenu.PopupMenuScrollSection();
			this.menu.addMenuItem(this._scroll_section);

			this._containers = new PopupMenu.PopupMenuSection();
			this._scroll_section.addMenuItem(this._containers);
			this._images = new PopupMenu.PopupMenuSection();
			this._scroll_section.addMenuItem(this._images);

			try {
				await Promise.all([this._show_containers(), this._show_images()]);
			} catch (e) {
				logError(e);
			}
		}

		async _show_containers() {
			// Check if show containers.
			if (this.settings.get_enum('show-value').toString() === "2") {
				return;
			}

			// Menu type.
			let menu = ContainerMenuText;
			if (this.settings.get_enum('menu-type').toString() === "1") {
				menu = ContainerMenuIcons;
			}

			return Docker.get_containers()
				.then((containers) => {
					if (containers.length === 0) {
						return;
					}

					this._containers.addMenuItem(new PopupMenu.PopupSeparatorMenuItem('Containers'));
					containers.forEach((container) => {
						container.settings = this.settings;
						this[container.id] = new menu.Container_Menu(container);
						this._containers.addMenuItem(this[container.id]);
					});
				});
		}

		async _show_images() {
			// Check if show images.
			if (this.settings.get_enum('show-value').toString() === "1") {
				return;
			}

			// Menu type.
			let menu = ImageMenuText;
			if (this.settings.get_enum('menu-type').toString() === "1") {
				menu = ImageMenuIcons;
			}

			return Docker.get_images()
				.then((images) => {
					if (images.length === 0) {
						return;
					}

					this._images.addMenuItem(new PopupMenu.PopupSeparatorMenuItem('Images'));
					images.forEach((image) => {
						image.settings = this.settings;
						this[image.id] = new menu.Image_Menu(image);
						this._images.addMenuItem(this[image.id]);
					});
				});
		}
	}
);
